import './App.css';
import Homepage from './pages/homepage';
import ReactDOM from 'react';

function App() {
  return (
    <div className="App">
      <Homepage />
    </div>
  );
}

export default App;

// ReactDOM.render(
//   <App />,
//   document.getElementById('root')
// );