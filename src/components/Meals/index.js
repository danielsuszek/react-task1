import React from 'react'
import Meal from '../Meal'

const Meals = ({arrMeals}) => {
  return(
    <div>
      <ul>
        {arrMeals.map((mealName, i) => (
          <Meal key={i} mealName={mealName}/>
        ))}
      </ul>
    </div>
  )
}

// class Meals extends React.Component {
//   render() {
//     return (
//       <div>
//         <ul>
//           {this.props.arrMeals.map((mealName, i) => (
//             <Meal key={i} mealName={mealName}/>
//           ))}
//         </ul>
//       </div>
//     )
//   }
// }

export default Meals