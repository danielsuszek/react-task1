import React from 'react'
const Meal = ({i, mealName}) => {
  return (
    <li key={i} >{mealName}</li>
  )
}

// class Meal extends React.Component {
//   render() {
//     return(
//       <li key={this.props.i}>{this.props.mealName}</li>
//     )
//   }
// }

export default Meal