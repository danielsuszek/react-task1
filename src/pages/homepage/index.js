import React from 'react'
import Header from '../../components/Header'
import Meals from '../../components/Meals'

const arrMeals = [
  'pizza', 'sushi', 'spagetti', 'pierogi', 'kopytka'
]

const homepage = () => {
  return (
    <div>
      <Header title='React Task 1' subtitle='Ulubione dania'/>
      <Meals arrMeals={arrMeals} />
    </div>
  )
}

export default homepage
